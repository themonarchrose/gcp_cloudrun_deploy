**GCP CloudRun deployment**

Deploys a service to GCP CloudRun

---

## Edit the envvars files or set these variables

The region you're deploying to
GCP_REGION=

Your GCP Project ID
GCP_PROJECT=

Your service account email

Note: This service account must have the following permissions

* Container Registry Service Agent
* Service Account User
* Cloud Run Admin

GCP_ACC_EMAIL=

The JSON key for your service account.

This needs to in base64 format. Convert the key to base64 by doing
```bash
cat yourkey.json | jq 'tostring' | base64 - | pbcopy
```
This will copy the key to your clipboard

GCP_ACC_KEY=

The image you wish to deploy

GCP_IMAGE=

The CloudRun service name

GCP_CLOUDRUN_NAME=

---

## Running the container

docker run -ti --env-file=envvars <imagename|id>

---
