FROM google/cloud-sdk:latest
WORKDIR /
RUN apt-get install jq -y && jq --version
COPY ./cloudrun.sh /opt/scripts/cloudrun.sh  
ENTRYPOINT [ "/opt/scripts/cloudrun.sh" ] 