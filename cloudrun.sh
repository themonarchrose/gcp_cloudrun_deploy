#!/bin/bash 
echo ${GCP_ACC_KEY} | base64 -d | jq -r 'fromjson' > gcpconfig.json
gcloud auth activate-service-account ${GCP_ACC_EMAIL} --key-file=gcpconfig.json
gcloud run deploy --region ${GCP_REGION} --image ${GCP_IMAGE} \
       --platform managed ${GCP_CLOUDRUN_NAME} --quiet  --project ${GCP_PROJECT}